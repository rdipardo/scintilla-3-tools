#!/bin/sh
set -e
find . -name \*mak -print | xargs -I% cp % %.orig
find . -name \*mak -print | xargs -I% sed -i '/CROSS_OSX =/ c\CROSS_OSX = o32-clang++ -I/opt/osx/include/c++/v1\' %
find . -name \*mak -print | xargs -I% sed -i 's/$(CROSS_OSX)$(CXX)/$(CROSS_OSX)/g' %
find . -name \*mak -print | xargs -I% sed -i 's/$(CROSS_OSX)$(AR)/i386-apple-darwin14-ar/g' %
"$@"
