#!/bin/sh
set -e
TASK="make -f check.mak"

if [ ! -f ./scintilla/check.mak ]; then
  hg clone http://hg.code.sf.net/p/scintilla/code scintilla
fi

cd scintilla
hg up LongTerm3 -C
/entry.sh echo "Setting up pre-commit hooks"
(
cat<<EOL
[ui]
username = John Doe <some1@example.com>
[hooks]
pretxncommit.adddeps = test -z $(hg status | grep '^A' | grep '\.\(h\|cxx\)$') || ${TASK} deps > /dev/null 2>&1
pretxncommit.compile = ${TASK}
EOL
) > .hg/hgrc

touch include/New.h
hg add include/New.h
hg commit -m "Backport: $(date -I'seconds')"
hg log | head
