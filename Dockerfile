#
# Distributed under the terms of the BSD Zero Clause License
# See the LICENSE file for more information
#

FROM ubuntu:14.04 AS build
LABEL maintainer="Robert Di Pardo <dipardo.r@gmail.com>"
LABEL license="0BSD"
LABEL description="Cross-compilation environment for validating patches \
to Scintilla version 3.x.x"

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

RUN apt-get -qq update \
 && apt-get -qqy --no-install-recommends install \
    make \
    pkg-config \
    clang \
    g++ \
    g++-mingw-w64-i686 \
    g++-multilib \
    libncurses5-dev \
    liblua5.1-0-dev \
    libgtk2.0-dev \
    libgtk-3-dev \
    libpyside-dev \
    libqt4-dev \
    python-pyside.qtgui \
    libshiboken-dev \
    shiboken \
    xz-utils \
    zip \
    unzip \
    tar \
    wget \
    sed \
    grep \
    findutils \
    mercurial \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

FROM multiarch/crossbuild:latest as mactools
ENV CROSS_TRIPLE i386-apple-darwin
ENV OSXCROSS_ENABLE_WERROR_IMPLICIT_FUNCTION_DECLARATION 1

FROM build
COPY --from=mactools /usr/osxcross /usr/osxcross
COPY --from=mactools /usr/i386-apple-darwin14 /usr/i386-apple-darwin14
COPY --from=mactools /usr/lib/x86_64-linux-gnu /usr/local/lib/x86_64-linux-gnu
RUN ln -s /usr/osxcross/SDK/MacOSX10.10.sdk/usr /opt/osx

ENV PATH $PATH:/usr/osxcross/bin:/usr/i386-apple-darwin14/bin
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/usr/osxcross/lib:/usr/local/lib/x86_64-linux-gnu
ENV OSXCROSS_NO_INCLUDE_PATH_WARNINGS 1

COPY ./entry.sh /
RUN chmod 744 /entry.sh
WORKDIR /scintilla
ENTRYPOINT ["/entry.sh"]
CMD ["make", "-f", "check.mak"]
