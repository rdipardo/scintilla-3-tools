[![Image Size][docker_image_size]][docker_hub_repo]
[![Docker Pulls][docker_pulls]][docker_hub_repo]

## Usage

Clone the Scintilla 3 branch with [mercurial](https://www.mercurial-scm.org),
as explained [here]["LongTerm3" branch].

To build the default `make` target (a cross-platform compile check), enter the
source tree and run the image:

    docker run --rm -v $(pwd):/scintilla -it rdipardo/scintilla-3-tools


To build a specific target, pass the complete invocation as an argument, e.g.:

    docker run --rm -v $(pwd):/scintilla -it rdipardo/scintilla-3-tools \
      make -f check.mak deps


Environment variables can be [overridden][Environment variables] with the `-e`
option, e.g.,

    docker run --rm -v $(pwd):/scintilla -e GTK3=1 -it rdipardo/scintilla-3-tools \
      make -f check.mak bin/scintilla_gtk.a


## Caveats

This image is **only** intended for compile-checking patches backported to
Scintilla's ["LongTerm3" branch][].

An image suitable for testing Scintilla 4 [is available][sjohannes/scintilla-test-deps],
although it hasn't been updated in some time.


## TODO

Many of the targets described in Scintilla's makefiles currently fail, partly
because of limitations explained in [the documentation][], as well as:

- the size constraints of a Docker image, making it impractical to provide, for
  example, a functional `wine` environment (needed by the `dmapp` target)

- inconsistent ISO standard compliance in the code base; for example, the
  `test` target runs a 4.8 GNU compiler on files that include [C++17 headers][]:

  ```
    testPerLine.cxx:6:23: fatal error: string_view: No such file or directory
      #include <string_view>
  ```

- hard-coded dependencies; e.g., the `bait` target fails if the `GTK3` variable
  is defined, because the test program is built from its own Makefile, which
  needs the `gtk+-2.0` development libraries


## Image Details

### Available compilers

#### GNU/Linux
- gcc version 4.8.4 (Ubuntu 4.8.4-2ubuntu1~14.04.4)
- clang version 3.4 (tags/RELEASE_34/final) (based on LLVM 3.4)

Executables: `gcc`, `g++`, `clang`, `clang++`

#### MS Windows
- mingw-w64-i686, gcc version 4.8.2 (GCC), posix thread model

Executables: `i686-w64-mingw32-gcc`, `i686-w64-mingw32-g++`

#### OSX/Darwin
- apple-darwin14, OSX SDK version 10.10, posix thread model

Executables: `o32-clang`, `o32-clang++`, `o64-clang`, ` o64-clang++`


## Third Party Notice

This image pulls its OSX cross-compiler from [multiarch/crossbuild][],
which is distributed under [the MIT License](https://github.com/multiarch/crossbuild/blob/master/LICENSE).


## License

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.


["LongTerm3" branch]: https://scintilla.sourceforge.io/LongTermDownload.html
[the documentation]: https://sourceforge.net/p/scintilla/code/ci/LongTerm3/tree/BACKPORTING
[Environment variables]: https://docs.docker.com/engine/reference/run/#env-environment-variables
[C++17 headers]: https://en.cppreference.com/w/cpp/header/string_view
[sjohannes/scintilla-test-deps]: https://hub.docker.com/r/sjohannes/scintilla-test-deps
[multiarch/crossbuild]: https://hub.docker.com/r/multiarch/crossbuild
[docker_image_size]: https://img.shields.io/docker/image-size/rdipardo/scintilla-3-tools?sort=date
[docker_pulls]: https://img.shields.io/docker/pulls/rdipardo/scintilla-3-tools?label=pulls
[docker_hub_repo]: https://hub.docker.com/r/rdipardo/scintilla-3-tools
